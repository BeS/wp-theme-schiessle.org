<?php

// enable featured images
add_theme_support( 'post-thumbnails');

// Widget Settings

if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'name' => 'First Column',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h4 class="widgettitle">',
	'after_title' => '</h4>',
	));

if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'name' => 'Second Column',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h4 class="widgettitle">',
	'after_title' => '</h4>',
	));

if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'name' => 'Third Column',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h4 class="widgettitle">',
	'after_title' => '</h4>',
	));

if ( function_exists('register_sidebar') )
	register_sidebar(array(
		'name' => 'Fourth Column',
		'before_widget' => '<li id="%1$s" class="widget %2$s">',
	'after_widget' => '</li>',
	'before_title' => '<h4 class="widgettitle">',
	'after_title' => '</h4>',
	));


$themename = "The Fundamentals of Graphic Design";
$shortname = "tfogd";
$options = array (

    array(  "name" => "Blog Position",
            "id" => $shortname."_blog_position",
            "type" => "select",
            "std" => "Left",
            "options" => array("Left", "Center")),

    array(  "name" => "Top Menu background color",
            "id" => $shortname."_menu_bgcolor",
	    "type" => "text",
            "std" => "#000000"),

    array(  "name" => "Top Menu mouseover background color",
            "id" => $shortname."_menu_over_bgcolor",
	    "type" => "text",
            "std" => "#118DA6"),

    array(  "name" => "Top Menu current page background color",
            "id" => $shortname."_menu_opened_bgcolor",
	    "type" => "text",
            "std" => "#CDEB8B"),

    array(  "name" => "Top Menu text link color",
            "id" => $shortname."_menu_text_color",
	    "type" => "text",
            "std" => "#ffffff")

);

function mytheme_add_admin() {

    global $themename, $shortname, $options;

    if ( $_GET['page'] == basename(__FILE__) ) {

        if ( 'save' == $_REQUEST['action'] ) {

                foreach ($options as $value) {
                    update_option( $value['id'], $_REQUEST[ $value['id'] ] ); }

                foreach ($options as $value) {
                    if( isset( $_REQUEST[ $value['id'] ] ) ) { update_option( $value['id'], $_REQUEST[ $value['id'] ]  ); } else { delete_option( $value['id'] ); } }

                header("Location: themes.php?page=functions.php&saved=true");
                die;

        } else if( 'reset' == $_REQUEST['action'] ) {

            foreach ($options as $value) {
                delete_option( $value['id'] ); }

            header("Location: themes.php?page=functions.php&reset=true");
            die;

        }
    }

    add_theme_page($themename." Options", "Current Theme Options", 'edit_themes', basename(__FILE__), 'mytheme_admin');

}

function mytheme_admin() {

    global $themename, $shortname, $options;

    if ( $_REQUEST['saved'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings saved.</strong></p></div>';
    if ( $_REQUEST['reset'] ) echo '<div id="message" class="updated fade"><p><strong>'.$themename.' settings reset.</strong></p></div>';

?>
<div class="wrap">
<h2><?php echo $themename; ?> settings</h2>

<form method="post">

<table class="optiontable">

<?php foreach ($options as $value) {

if ($value['type'] == "text") { ?>

<tr valign="top">
    <th scope="row"><?php echo $value['name']; ?>:</th>
    <td>
        <input name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>" type="<?php echo $value['type']; ?>" value="<?php if ( get_settings( $value['id'] ) != "") { echo get_settings( $value['id'] ); } else { echo $value['std']; } ?>" />
    </td>
</tr>

<?php } elseif ($value['type'] == "select") { ?>

    <tr valign="top">
        <th scope="row"><?php echo $value['name']; ?>:</th>
        <td>
            <select name="<?php echo $value['id']; ?>" id="<?php echo $value['id']; ?>">
                <?php foreach ($value['options'] as $option) { ?>
                <option<?php if ( get_settings( $value['id'] ) == $option) { echo ' selected="selected"'; } elseif ($option == $value['std']) { echo ' selected="selected"'; } ?>><?php echo $option; ?></option>
                <?php } ?>
            </select>
        </td>
    </tr>

<?php
}
}
?>

</table>

<p class="submit">
<input name="save" type="submit" value="Save changes" />
<input type="hidden" name="action" value="save" />
</p>
</form>
<form method="post">
<p class="submit">
<input name="reset" type="submit" value="Reset" />
<input type="hidden" name="action" value="reset" />
</p>
</form>

<?php
}

function mytheme_wp_head() { ?>
<link href="<?php bloginfo('template_directory'); ?>/style.php" rel="stylesheet" type="text/css" />
<?php }

add_action('wp_head', 'mytheme_wp_head');
add_action('admin_menu', 'mytheme_add_admin');

?>
