<div id="sidebar">
<ul id="first">
<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('First Column') ) : else : ?>
<li id="links" class="widget">
	<h4>Links</h4>
	<ul>
		<?php wp_list_bookmarks('title_li='); ?>
	</ul>
</li>

	<li id="meta" class="widget">
	<h4>Meta</h4>	<ul>
	<?php wp_register(); ?>
	<li><?php wp_loginout(); ?></li>
	<li><a href="http://validator.w3.org/check/referer" title="This page validates as XHTML 1.0 Transitional">Valid <abbr title="eXtensible HyperText Markup Language">XHTML</abbr></a></li>
	<li><a href="http://gmpg.org/xfn/"><abbr title="XHTML Friends Network">XFN</abbr></a></li>
	<li><a href="http://wordpress.org/" title="Powered by WordPress, state-of-the-art semantic personal publishing platform.">WordPress</a></li>
	<li><a href="http://wordpress.de/" title="Deutsche WordPress Community">WordPress.de</a></li>
	<?php wp_meta(); ?>
	</ul>
</li>

<?php endif; ?>
</ul>

<ul id="second">
<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('Second Column') ) : else : ?>

		<li id="categories" class="widget">
                <h4>Categories</h4>
		<ul>
		<?php wp_list_cats('hierarchical=1'); ?>
		</ul>
</li>
<?php endif; ?>
</ul>

<ul id="third">
<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('Third Column') ) : else : ?>

<li id="archives" class="widget">
		<h4>Archives</h4>
		<ul>
		<?php wp_get_archives('type=monthly'); ?>
		</ul>
		</li>
<?php endif; ?>
</ul>

<ul id="fourth">
<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar('Fourth Column') ) : else : ?>
<li id="feed" class="widget">
	<h4>Feed Rss</h4>
	<ul><li><a href="feed:<?php bloginfo('rss2_url'); ?>">Entries (RSS)</a></li>
	<li><a href="feed:<?php bloginfo('comments_rss2_url'); ?>">Comments (RSS)</a></li>
	</ul>
</li>

<li id="search_in_sidebar" class="widget">
<h4>Search</h4>
<?php get_search_form('title='); ?>
</li>
<!--
<li class="widget">
<h4>Meta</h4>
<a href="http://blog.schiessle.org/wp-login.php">Log in</a>
</li>
-->


<?php endif; ?>
</ul>
</div>
<span class="clear"></span>

