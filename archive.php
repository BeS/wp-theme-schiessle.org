<?php get_header(); ?>
<!--include sidebar-->
<?php get_sidebar('pages'); ?>
	<!--the loop-->
<div id="posts">
		<?php if (have_posts()) : ?>

<div id="kindofarchive">
		 <?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
<?php /* If this is a category archive */ if (is_category()) { ?>
		Archive for <em><?php echo single_cat_title(); ?></em>

 	  <?php /* If this is a daily archive */ } elseif (is_day()) { ?>
		Archive for <?php the_time('F jS, Y'); ?>

	 <?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
		Archive for <?php the_time('F, Y'); ?>

		<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
		Archive for <?php the_time('Y'); ?>

	  <?php /* If this is a search */ } elseif (is_search()) { ?>
		Search Results

	  <?php /* If this is an author archive */ } elseif (is_author()) { ?>
		Author Archive

		<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
		Blog Archives

               <!--do not delete-->
		<?php } ?>

</div>

                <!--loop article begin-->
		<?php while (have_posts()) : the_post(); ?>
	<div class="post">
			<!--post title as a link-->
				<h2 class="title" id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>


				<!--post text with the read more link-->
<?php if(!empty($post->post_excerpt)) { ?>
	<div class="excerpt"><?php the_excerpt(); ?></div>
<?php } else { ?>
<br />
<?php } ?>

<div class="post-text">

					<?php the_content('Read the rest of this entry &raquo;'); ?>

<div class="post-meta">
					<!--you need trackback rdf for pings from non wp blogs do not delete the html comments they are necessary-->
		 <!--<?php trackback_rdf(); ?>-->
				<!--show  tags categories, edit link ,comments-->

                  <!--post time-->
 <span class="meta-date"><?php the_time('F jS, Y') ?><?php edit_post_link('Edit', ' (', ')'); ?></span>
 <?php the_tags('<span class="meta-tags">', '', ''); the_category(' ');?></span><br />
 <span class="meta-comments"><?php comments_popup_link('No Comments &#187;', '<strong>1 Comment &#187;</strong>', '<strong>% Comments &#187;</strong>'); ?></span><br />
</div>
	</div>
</div>
	        <!--end of one post-->
		<?php endwhile; ?>

		<!--navigation--><div id="navlinks">
                <?php next_posts_link('&laquo; Previous Entries') ?>
		<?php previous_posts_link('Next Entries &raquo;') ?>
		</div><span class="clear"></span>

	<!-- do not delete-->
	<?php else : ?>
<div class="text-feedback">
		Not Found<br /><br />
<?php get_search_form('title='); ?></div>
		<?php  // include (TEMPLATEPATH . '/searchform.php'); ?>

         <!--do not delete-->
	<?php endif; ?>

	</div>
<!--archive.php end-->

<!-- include sidebar -->
<?php get_sidebar('main'); ?>
<!--include footer-->
<?php get_footer(); ?>
