<?php

require_once( dirname(__FILE__) . '../../../../wp-config.php');
require_once( dirname(__FILE__) . '/functions.php');
header("Content-type: text/css");

global $options;

foreach ($options as $value) {
  if (get_settings( $value['id'] ) === FALSE) {
     $$value['id'] = $value['std'];
  } else {
     $$value['id'] = get_settings( $value['id'] );
  }
}
?>
body {

	<?php if($tfogd_blog_position==="Center") {
	echo "text-align: center;";
	} ?>
}
#page {

	<?php if($tfogd_blog_position==="Center") {
	echo "text-align: left; margin: 0 auto;";
	} ?>

	<?php if($tfogd_blog_position==="Left") {
	echo "margin: 0px; margin-left: 10px;";
	} ?>
}
#sidebar-pages {
	background-color: <?php echo $tfogd_menu_bgcolor; ?>;
}
#sidebar-pages a {
	color: <?php echo $tfogd_menu_text_color; ?>;;

}
#sidebar-pages a:hover {
	background-color: <?php echo $tfogd_menu_over_bgcolor; ?>;
}
.current_page_item {
	background-color: <?php echo $tfogd_menu_opened_bgcolor; ?>;
}
#sidebar-pages a:hover {
	border-bottom: 2px solid <?php echo $tfogd_menu_bgcolor; ?>;
}


