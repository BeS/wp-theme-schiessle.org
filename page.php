<?php get_header(); ?>
<?php get_sidebar('pages'); ?>
<!--page.php-->
    <!--loop-->
<div id="posts">
 <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="post">
			<!--post title as a link-->
				<h2 class="title-single" id="post-<?php the_ID(); ?>"><?php the_title(); ?></h2>


				<!--post excerpt-->
<?php if(!empty($post->post_excerpt)) { ?>
	<div class="excerpt"><?php the_excerpt(); ?></div>
<?php } else { ?>
<br />
<?php } ?>


<div class="post-text">

					<?php the_content('Read the rest of this entry &raquo;'); ?>

	                       <!--if you paginate pages-->
				<?php link_pages('<p><strong>Pages:</strong> ', '</p>', 'number'); ?>
</div> </div> </div>
	<!--end of post and end of loop-->
	  <?php endwhile; endif; ?>

         <!--edit link-->
	<?php edit_post_link('Edit this entry.', '<p>', '</p>'); ?>


<!--page.php end-->
<!--include sidebar-->

<!-- include sidebar -->
<?php get_sidebar('main'); ?>
<!--include footer-->
<?php get_footer(); ?>
