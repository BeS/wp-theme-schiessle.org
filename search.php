<?php get_header(); ?>
<!--include sidebar-->
<?php get_sidebar('pages'); ?>
<!--search.php-->
<div id="posts">
        <!--loop-->
	<?php if (have_posts()) : ?>

	<div class="text-feedback">Search Results </div>

                <!--loop-->
                <!--loop article begin-->
		<?php while (have_posts()) : the_post(); ?>
	<div class="post">
			<!--post title as a link-->
				<h2 class="title" id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>


				<!--post text with the read more link-->
<?php if(!empty($post->post_excerpt)) { ?>
	<div class="excerpt"><?php the_excerpt(); ?></div>
<?php } else { ?>
<br />
<?php } ?>

<div class="post-text">

					<?php the_content('Read the rest of this entry &raquo;'); ?>

<div class="post-meta">
					<!--you need trackback rdf for pings from non wp blogs do not delete the html comments they are necessary-->
		 <!--<?php trackback_rdf(); ?>-->
				<!--show  tags categories, edit link ,comments-->

                  <!--post time-->
 <span class="meta-date"><?php the_time('F jS, Y') ?><?php edit_post_link('Edit', ' (', ')'); ?></span>
 <?php the_tags('<span class="meta-tags">', '', ''); the_category(' ');?></span><br />
 <span class="meta-comments"><?php comments_popup_link('No Comments &#187;', '<strong>1 Comment &#187;</strong>', '<strong>% Comments &#187;</strong>'); ?></span><br />
</div>
	</div>
</div>
	        <!--end of one post-->
		<?php endwhile; ?>

		<!--navigation--><div id="navlinks">
                <?php next_posts_link('&laquo; Previous Entries') ?>
		<?php previous_posts_link('Next Entries &raquo;') ?>
		</div><span class="clear"></span>


        <!--necessary do not delete-->
	<?php else : ?>
<div class="text-feedback">
		No posts found. Try a different search?<br /><br />
<?php get_search_form('title='); ?>
</div>
                 <!--include searchform-->
		<?php // include (TEMPLATEPATH . '/searchform.php'); ?>
        <!--do not delete-->
	<?php endif; ?>
	</div>

<!--search.php end-->

<!-- include sidebar -->
<?php get_sidebar('main'); ?>
<!--include footer-->
<?php get_footer(); ?>
