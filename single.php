<?php get_header(); ?>
<?php get_sidebar('pages'); ?>
<!--single.php-->
<div id="posts">
<!--loop-->
  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="post">
			<!--post title as a link-->
				<h2 class="title-single" id="post-<?php the_ID(); ?>"><?php the_title(); ?></h2>


				<!--post excerpt-->
<?php if(!empty($post->post_excerpt)) { ?>
	<div class="excerpt"><?php the_excerpt(); ?></div>
<?php } else { ?>
<br />
<?php } ?>


<div class="post-text">

					<?php the_content('Read the rest of this entry &raquo;'); ?>

<div class="post-meta">
					<!--you need trackback rdf for pings from non wp blogs do not delete the html comments they are necessary-->
		 <!--<?php trackback_rdf(); ?>-->
				<!--show  tags categories, edit link ,comments-->

                  <!--post time-->
 <span class="meta-date"><?php the_time('F jS, Y') ?><?php edit_post_link('Edit', ' (', ')'); ?></span>
 <?php the_tags('<span class="meta-tags">', '', ''); the_category(' ');?></span><br />
 <span class="meta-comments"><?php comments_popup_link('No Comments &#187;', '<strong>1 Comment &#187;</strong>', '<strong>% Comments &#187;</strong>'); ?></span><br />
You can follow any responses to this entry through the <?php comments_rss_link('RSS 2.0'); ?> feed.</span>

						<?php if (('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
							// Both Comments and Pings are open ?>
							You can <a href="#respond">leave a response</a>, or <a href="<?php trackback_url(true); ?>" rel="trackback">trackback</a> from your own site.

						<?php } elseif (!('open' == $post-> comment_status) && ('open' == $post->ping_status)) {
							// Only Pings are Open ?>
							Responses are currently closed, but you can <a href="<?php trackback_url(true); ?> " rel="trackback">trackback</a> from your own site.

						<?php } elseif (('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
							// Comments are open, Pings are not ?>
							You can skip to the end and leave a response. Pinging is currently not allowed.

						<?php } elseif (!('open' == $post-> comment_status) && !('open' == $post->ping_status)) {
							// Neither Comments, nor Pings are open ?>
							Both comments and pings are currently closed.

						<?php } edit_post_link('Edit this entry.','',''); ?>



</div>
	</div>   </div>

	        <!--navigation-->
	<!--	<?php previous_post_link('&laquo; %link') ?>
		<?php next_post_link('%link &raquo;') ?> -->

<!--all options over and out-->
			<!--you need trackback rdf for pings from non wp blogs do not delete the html comments they are necessary-->
		 <!--<?php trackback_rdf(); ?>-->

	<!--include comments template-->
	<?php comments_template(); ?>

        <!--do not delete-->
	<?php endwhile; else: ?>
	<div class="text-feedback">
	Sorry, no posts matched your criteria.<br /><br />
<?php get_search_form('title='); ?>
</div>
<!--do not delete-->
<?php endif; ?>

	</div>
<!--single.php end-->

<!-- include sidebar -->
<?php get_sidebar('main'); ?>
<!--include footer-->
<?php get_footer(); ?>
